"""
"getGPS"
A script for wrangling GPS data from a USB device

Dean Meyer 2020
"""

import serial
import serial.tools.list_ports as port
import numpy as np
import time
import sys
import matplotlib.pyplot as plt
import pandas as pd

""" Find COM ports, display them, return user selection """
def selectPort():
    ports = port.comports()
    portsDevices = [n.device for n in ports]
    portsDesc = [n.description for n in ports]
    menu = ['*'+'  '+portsDesc[n] for n in range(len(portsDesc))]
    for n in menu:
        print(n)
    try:
        selection = input('Insert the name of a COM port (ex. COM5): ')
        if selection not in portsDevices:
            raise Exception
    except FileNotFoundError:          ###
        print('\nCOM port not found')  ###Need to change these 2 lines to COMport error 
    return selection

""" Ask for the team name """
def selectTeam():
    name = input('Enter the name of your team: ')
    return name

""" Waiting animation """
def waiting():
    animation = '|/-\\'
    for i in range(100):
        time.sleep(0.1)
        sys.stdout.write('\r' + animation[i % len(animation)])
        sys.stdout.flush()
    return None

""" Open a file, write to it, close it, return its name """
def logger(ser, name):    
    print('Press ctrl+c to stop GPS logging...')    
    while 1:
        try:            
            line = ser.readline().decode('utf-8')
            spl = line.split(sep=',')
            if spl[0] == '$GPRMC' and spl[2] == 'A':
                fname = name + '_' + spl[9] + '.csv'
                f = open(fname, 'a', newline='')
                newLine = name + ',' + formatGPRMC(line)
                f.write(newLine)
                f.close()
            
#            waiting()
            
        except KeyboardInterrupt:
            #stop the script, close the file and serial connection
            f.close()
            ser.close()
            return fname
            break

""" Format raw NMEA GPRMC line """
def formatGPRMC(rawLine):
    parts = rawLine.split(sep=',')
    rawTime = parts[1]
    rawLat = float(parts[3])
    rawLon = float(parts[5])
    speed = float(parts[7])
    course = float(parts[8])
    
    lat = toDecDeg(rawLat)
    lon = toDecDeg(rawLon) 
    
    # make coords negative based on hemisphere
    ns = parts[4]
    if ns == 'S':
        lat = lat * -1 #in case UAH deploys south of the equator
    ew = parts[6]
    if ew == 'W':
        lon = lon * -1
        
    # assemble a list and format to string
    pack = [rawTime, lat, lon, speed, course]
    packStr = [str(n) for n in pack]
    
    # put it all back together in one string
    newLine = ','.join(packStr) + '\n'
    return newLine

""" Convert lat/lon from DM.m to D.d format """
def toDecDeg(coord):
    conv = int(coord/100) + coord/60 - (5/3)*int(coord/100)
    return conv

""" Plot a ground trace of lat/lon data """
def plotData(file):
    hdr = ['id','time','lat','lon','spd','dir']
    df = pd.read_csv(file, names=hdr)
    ax = df.plot('lon','lat', kind='scatter', style='-o')
    fig = ax.get_figure()
    fig.savefig(file.split(sep='.')[0] + '.png')
    return None

selection = selectPort()
team = selectTeam()
ser = serial.Serial(port=selection, 
                  baudrate=4800, 
                  timeout=None, 
                  parity=serial.PARITY_NONE, 
                  stopbits=serial.STOPBITS_ONE, 
                  bytesize=serial.EIGHTBITS)
file = logger(ser, team)
plotData(file)