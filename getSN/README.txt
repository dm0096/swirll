## Dean Meyer 2020 ##
~~~~~~~~~
~ getSN ~
~~~~~~~~~
This program generates GR-compatible placefiles from SpotterNetwork.
It updates a placefile, selectedTeams.txt, every minute with objects
taken from the SpotterNetwork feed. The script will search by callsign
with signs taken from teamNames.txt, which the user may edit as they
please. 

The format for teamNames.txt shall be:

Paul Markowski
Josh Wurman
Reed Timmer
KL4GVA
...

Update your teamNames.txt first! Navigate to 
~/getSN/dist/ and edit teamNames.txt. To run, execute getSN.exe in the 
same directory. A command prompt window will appear. Press ctrl+c to stop, 
or exit the window to end operations. 

**This script will run every minute forever until you stop it!**

Once getSN generates a selectedTeams.txt in ~/getSN/dist/, select that
file in GR2Analyst's Placefile Manager. As long as getSN runs, it will 
update that file for GR2Analyst to read.