import requests
import datetime

u = 'http://192.168.4.106/cgi-bin/chm-cgi/'
date = datetime.datetime.today()
r = f"{date:%Y%m%d}"
l = '_Berm_CHMstddrd_000.nc?getfile'
url = u + r + l
filename = r + '_Berm_CHMstddrd_000.nc'
savePath = 'C:/Ceilometer/data/'

#download a .nc file straight from the server if url is given
r = requests.get(url)
with open(savePath + filename, 'wb') as f:
    f.write(r.content)