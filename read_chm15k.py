'''
Python script to read in UAH chm 15K ceilometer netCDF files
'''

import numpy as np
import matplotlib.pyplot as plt
import glob
import os
import ntpath
from netCDF4 import Dataset, num2date

#open ceilometer file
filepath = glob.glob('C:/Ceilometer/data/*.nc')[-1] #get latest .nc file
filename = os.path.splitext(ntpath.basename(filepath))[0]
file = Dataset(filepath) 
time = file.variables['time']
beta_raw = file.variables['beta_raw'][:]/1000000 #raw backscatter divided by 1e6 for ease of viewing
hght = file.variables['range'][:] /1000 #range is in m. Divided by 1000 to convert to km
beta_raw = beta_raw.T #transposes height to y-axis
beta_hr = file.variables['beta_raw_hr'][:] #high res backscatter
beta_hr = beta_hr.T #transposes height to y-axis
alt = file.variables['altitude'] #altitude of ceilometer MSL

#convert from epoch time and format HHMM
dtime = num2date(time[:], time.units) #converts time UTC time
dtime_f = np.array([f"{t:%H%M}" for t in dtime])

#plot ceilometer data
cmap = plt.cm.get_cmap('Spectral_r')
cmap.set_over('k')
cmap.set_under('w')
plotProps = {'cmap':cmap, 'vmin':-1, 'vmax':150}
cbarProps = {'label':'Backscatter (*1000000)', 'extend':'both'}
tickProps = {'ticks':dtime_f[::240], 'rotation':45} #stride is for how many time labels

fig = plt.figure(figsize=(10,5))
ax = fig.add_subplot(111)
m = plt.pcolormesh(dtime_f, hght, beta_raw, **plotProps)
plt.xticks(**tickProps) 
ax.set_ylabel('AGL (km)')
ax.set_ylim(0,4) #height range
ax.set_xlabel('UTC')
ax.set_title('Ceilometer Backscatter 4km')
plt.colorbar(**cbarProps)
plt.tight_layout()
plt.savefig('C:/Ceilometer/images/'+filename+'_'+dtime_f[-1]+'_4k')
plt.close()

fig2 = plt.figure(figsize=(10,5))
ax2 = fig2.add_subplot(111)
m2 = plt.pcolormesh(dtime_f, hght, beta_raw, **plotProps)
plt.xticks(**tickProps)
ax2.set_ylabel('AGL (km)')
ax2.set_ylim(0,15) #height range
ax2.set_xlabel('UTC')
ax2.set_title('Ceilometer Backscatter 15km')
plt.colorbar(**cbarProps)
plt.tight_layout()
plt.savefig('C:/Ceilometer/images/'+filename+'_'+dtime_f[-1]+'_15k')
plt.close()