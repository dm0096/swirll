import datetime
import requests
import time
import csv
    
def requestSN():
    # get GR placefile with SpotterNetwork locations
    url = 'http://www.spotternetwork.org/feeds/gr.txt'
    r = requests.get(url).text

    # list each "object"
    objs = r.split(sep='Object: ')
    hdr = objs[0] # extract the file header
    objs = objs[1:]

    return objs, hdr

def callsign(obj):
    # get the callsign from an "object"
    attrs = obj.split(sep='\n')
    if len(attrs) == 5:
        return attrs[2].split(sep=',')[3].split(sep='"')[1]
    else:
        return None
    
def locate(sieved):
    # given a list of GR "objects," return a list of lat/lon pairs
    return [[float(n) for n in o[0].split(sep='\n')[0].split(sep=',')] for o in sieved]

def getNames(sieved):
    # return a list of all names/callsigns given a list of objs
    return [callsign(n[0]) for n in sieved]

def getTimes(sieved):
    return [s[0].split(sep='\n')[1].split(sep='\\n')[1].split(sep=' ')[0:2] for s in sieved]
    
def selectionFromFile():
    # reads names from a file into a list of strings
    with open('teamNames.txt', 'r') as f:
        return f.read().splitlines()

def sieve(objs, selection):
    # return an object if its callsign is in a given list of callsigns
    return [[o for o in objs if callsign(o) == s] for s in selection]
        
def rejoin(objs, hdr):
    return hdr + 'Object: ' + 'Object: '.join([o[0] for o in objs])

def writer(rejoined):
    # write rejoined data to file
    with open('selectedTeams.txt', 'w') as f:
        f.write(rejoined)
    return None

def logger(names, locs, times):
    # write to a logfile with location and name data
    now = datetime.datetime.utcnow().date().isoformat()
    for i in range(len(names)):
        fname = names[i].replace(' ','_') + '_' + now + '.csv'
        rows = [times[i] + locs[i] for i in range(len(locs))]
        with open(fname, 'a', newline='') as f:
            wr = csv.writer(f)
            wr.writerow(rows[i])
    return None

#%%

# run the script every minute until stopped
print('Press ctrl+c to stop')
while True:
    try:
        objs, hdr = requestSN()
        selection = selectionFromFile()
        sieved = sieve(objs, selection)
        rejoined = rejoin(sieved, hdr)
        writer(rejoined)
        names = getNames(sieved)
        locs = locate(sieved)
        times = getTimes(sieved)
        logger(names, locs, times)
        time.sleep(60)
    except KeyboardInterrupt:
        break