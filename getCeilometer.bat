:: Dean Meyer 2020
::
:: "getCeilometer"
:: A script to pull data from a ceilometer

@echo OFF
TITLE getCeilometer
call C:\ProgramData\Anaconda3\Scripts\activate.bat

echo Getting data ...
C:/ProgramData/Anaconda3/python.exe C:/Ceilometer/scrape_chm15k.py 
echo Successful!

TIMEOUT /T 300 /NOBREAK
echo Plotting data ...
C:/ProgramData/Anaconda3/python.exe C:/Ceilometer/read_chm15k.py %*
if NOT ["%errorlevel%"]==["0"] pause
echo Successful!